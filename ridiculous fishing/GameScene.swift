//
//  GameScene.swift
//  ridiculous fishing
//
//  Created by Mahammad on 2019-10-21.
//  Copyright © 2019 Mahammad. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    var worldNode : SKNode?
    var spritenode : SKSpriteNode?
    var nodeTileWidth: CGFloat = 0
    var xOrgPosition : CGFloat = 0
    
    
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        worldNode = SKNode()
        self.addChild(worldNode!)
        let topNode = SKSpriteNode(imageNamed: "topWater")
        let middleNode = SKSpriteNode(imageNamed: "middleWater")
        let bottomNode = SKSpriteNode(imageNamed: "bottomWater")
        nodeTileWidth = topNode.frame.size.height
        topNode.anchorPoint = CGPoint(x: 0, y: 0)
        topNode.position = CGPoint(x: 0, y: 0)
        middleNode.anchorPoint = CGPoint(x: 0, y: 0)
        middleNode.position = CGPoint(x: 0, y: nodeTileWidth)
        bottomNode.anchorPoint = CGPoint(x: 0, y: 0)
        bottomNode.position = CGPoint(x: 0, y: nodeTileWidth)
        worldNode!.addChild(topNode)
        worldNode!.addChild(middleNode)
        worldNode!.addChild(bottomNode)
        



        let m1 = SKAction.moveBy(x: -15, y: 0, duration: 0.5)

        let m2 = SKAction.moveBy(x: 15, y: 0, duration: 0.5)

     
        let moveWave = SKAction.sequence([m1, m2])
        self.enumerateChildNodes(withName: "wave") {
            (node, stop) in
            let wave = node as! SKSpriteNode
            wave.run(SKAction.repeatForever(moveWave))
        }
        }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            let xTouchPosition = touch.location(in: self).y
            if xOrgPosition != 0.0{
                let xNewPosition = worldNode!.position.y + (xOrgPosition  - xTouchPosition)
                
                if xNewPosition <= -(2 * nodeTileWidth)
                {
                    worldNode!.position = CGPoint(x: 0, y: 0)
                    print("bottom reached")
                }
                else if xNewPosition >= 0 {
                    worldNode!.position = CGPoint(x: 0, y: -(2 * nodeTileWidth))
                    print("top reached")
                }else {
                    worldNode!.position = CGPoint(x: 0, y: xNewPosition)
                }
                if xTouchPosition < xOrgPosition {
                    spritenode?.zRotation = CGFloat(Double.pi/2.0)
                }else
                {
                    spritenode?.zRotation = -CGFloat(Double.pi/2.0)
                }
            }
            xOrgPosition = xTouchPosition
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        xOrgPosition = 0
    }
    override func update(_ currentTime: TimeInterval) {
        
    }
    func didBegin(_ contact: SKPhysicsContact) {
        if let scene = SKScene(fileNamed: "level") {
            scene.scaleMode = .aspectFill
            // OPTION 1: Change screens with an animation
            self.view?.presentScene(scene, transition: SKTransition.flipVertical(withDuration: 2.5))
            // OPTION 2: Change screens with no animation
            //self.view?.presentScene(scene)
            
            func didBegin(_ contact: SKPhysicsContact) {
//            let nodeA = contact.bodyA.node
//            let nodeB = contact.bodyB.node
//
//            if (nodeA == nil || nodeB == nil) {
//                return
//            }
//                       if (nodeA!.name == "sea" && nodeB!.name == "hook") {
//
//                        // show new LEVEL
//                        // Load 'level.sks'
//                if let scene = SKScene(fileNamed: "level") {
//                    scene.scaleMode = .aspectFill
//                    // OPTION 1: Change screens with an animation
//                    self.view?.presentScene(scene, transition: SKTransition.flipVertical(withDuration: 2.5))
//                    // OPTION 2: Change screens with no animation
//                    //self.view?.presentScene(scene)
//                }
                
                
            }
           
        }
        
        
        
        
    }
    }
  
